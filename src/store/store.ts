// Add all objects with data into this directory
import { IStore, Pokemon } from "../model/model";

export const store: IStore = {
    lastLoadCountCaught: 0,
    next: "",
    pokemons: new Array<Pokemon>,
    caughtPokemons: new Array<Pokemon>,
    fetching: true,
    shouldLoad: true, 
}