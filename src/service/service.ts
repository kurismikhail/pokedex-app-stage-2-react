import { IService, Pokemon } from "../model/model";
import { store } from "../store/store";
import { FetchPokemons } from "../transport/transport";
import { formatDate } from "../utils/formatDate";

class Service implements IService{

    public async getPokemonsList(): Promise<void> {
        if (store.fetching && store.shouldLoad) {
            store.fetching = false;
            let response = await this.fetchPokemons.getPokemonsList(store.next);
            const data = Object.values(response)[0]
            store.next = data.next;
            if (!store.next) {
                store.shouldLoad = false;
            }
            const result = data.results;
            for (let i = 0; i < result.length; i++) {
                const currentPokemon = await this.fetchPokemons.getDefinitePokemonByUrl(result[i].url);
                const data = Object.values(currentPokemon)[0]
        
                const isCaught: Pokemon | undefined = store.caughtPokemons ? store.caughtPokemons.find(pokemon => pokemon.id === data.id) : undefined;

                store.pokemons.push({
                    id: data.id,
                    name: data.name,
                    avatar: data.sprites.other.home.front_default,
                    isCaught: isCaught !== undefined,
                    abilities: Object.values(data.abilities),
                    date: ''
                })
            }   
            store.fetching = true;
        }
    }

    public handleCaught (id: number): void {

        const caughtPokemon: Pokemon | undefined = store.pokemons.find(pokemon => pokemon.id === id);
        if (caughtPokemon) {
            caughtPokemon.isCaught = true;
            store.caughtPokemons.push({
                id: caughtPokemon.id,
                name: caughtPokemon.name,
                avatar: caughtPokemon.avatar,
                isCaught: true,
                abilities: caughtPokemon.abilities,
                date: formatDate(new Date())
            })
            localStorage.setItem('caughtPokemons', JSON.stringify(store.caughtPokemons));
        }
    };
    
    public initCaughtPokemons (): void {
        const storage = localStorage.getItem('caughtPokemons');
        if (storage) store.caughtPokemons = JSON.parse(storage);
    }

    public getPokemonById(id: number) {
        let pokemon = store.caughtPokemons.find(pokemon => pokemon.id === id);
        if(pokemon) return pokemon
        pokemon = store.pokemons.find(pokemon => pokemon.id === id);
        if (pokemon) return pokemon
    }

    public async loadPokemonById(id: number): Promise<Pokemon> {
        store.fetching = false;
        const response = await this.fetchPokemons.getDefinitePokemonById(id);
        const data = Object.values(response)[0]
        store.fetching = true;
        return {
            id: data.id,
            name: data.name,
            avatar: data.sprites.other.home.front_default,
            isCaught: false,
            abilities: data.abilities,
            date: ''
       }
    }

    constructor(
        private fetchPokemons: FetchPokemons
    ) {}
}

export const service: Service = new Service(
    new FetchPokemons()
)